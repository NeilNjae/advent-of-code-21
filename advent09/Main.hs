-- Writeup at https://work.njae.me.uk/2021/12/09/advent-of-code-2021-day-9/

import Data.Array 
import Data.Char
import Data.List (sort)
import qualified Data.Set as S
import Data.Set ((\\))
import Linear (V2(..), (^+^))

type Coord = V2 Int
type Grid = Array Coord Int
type Basin = S.Set Coord

main :: IO ()
main = 
  do  text <- readFile "data/advent09a.txt"
      let grid = mkGrid text
      print $ part1 grid
      print $ part2 grid

mkGrid :: String -> Grid
mkGrid text = listArray ((V2 0 0), (V2 r c)) $ map digitToInt $ concat rows
  where rows = lines text
        r = length rows - 1
        c = (length $ head rows) - 1

part1 :: Grid -> Int
part1 grid = sum $ map (riskLevel grid) $ lowPoints grid

part2 :: Grid -> Int
part2 grid = product $ take 3 ordSizes
  where lows = lowPoints grid
        sizes = map (basinSize grid) lows
        ordSizes = reverse $ sort sizes

riskLevel :: Grid -> Coord -> Int
riskLevel grid here = grid ! here + 1

lowPoints :: Grid -> [Coord]
lowPoints grid = filter (isLow grid) $ indices grid

isLow :: Grid -> Coord -> Bool
isLow grid here = all (> this) nbrs
  where nbrs = map (grid ! ) $ neighbours grid here
        this = grid ! here

higherNeighbours :: Grid -> Coord -> [Coord]
higherNeighbours grid here = filter isHigher $ neighbours grid here
  where this = grid ! here
        isHigher there = (grid ! there) > this

basinSize :: Grid -> Coord -> Int
basinSize grid basinSeed = S.size $ breadthFirstSearch grid (S.singleton basinSeed) S.empty

breadthFirstSearch :: Grid -> Basin -> Basin -> Basin
breadthFirstSearch grid agenda basin
  | S.null agenda = basin
  | otherwise = breadthFirstSearch grid agenda' basin'
  where here = S.findMin agenda
        candidates = (S.fromList $ higherNeighbours grid here) \\ basin
        basin' = if (grid ! here) == 9 
                 then basin
                 else S.insert here basin
        agenda' = S.union candidates $ S.delete here agenda

neighbours :: Grid -> Coord -> [Coord]
neighbours grid here = filter (inRange (bounds grid))  
  [ here ^+^ delta 
  | delta <- [V2 -1 0, V2 1 0, V2 0 -1, V2 0 1]
  ]
