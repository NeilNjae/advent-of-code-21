-- Writeup at https://work.njae.me.uk/2021/12/16/advent-of-code-2021-day-14/

import Data.Text ()
import qualified Data.Text.IO as TIO

import Data.Attoparsec.Text
import Control.Applicative

import Data.List
import qualified Data.Map.Strict as M
import Data.Map.Strict ((!))
import qualified Data.MultiSet as MS
import qualified Data.Set as S

type RuleSet = M.Map String String
type PolyPairs = MS.MultiSet String


main :: IO ()
main = 
  do  text <- TIO.readFile "data/advent14.txt"
      let (template, rules) = successfulParse text
      print $ part1 rules template
      print $ part2 rules template

part1 :: RuleSet -> String -> Int
part1 rules template = (last counts) - (head counts)
  where result = (simulateNaive rules template) !! 10
        counts = sort $ map snd $ MS.toOccurList $ MS.fromList result

simulateNaive :: RuleSet -> String -> [String]
simulateNaive rules polymer = iterate (stepNaive rules) polymer

stepNaive :: RuleSet -> String -> String
stepNaive rules polymer = merge polymer $ concatMap (rules !) $ mkPairs polymer

part2 :: RuleSet -> String -> Int
part2 rules template = (last counts) - (head counts)
  where pairs = MS.fromList $ mkPairs template
        result = (simulate rules pairs) !! 40
        elementCounts = countElements result
        -- counts = sort $ map snd $ MS.toOccurList elementCounts
        counts = sort $ M.elems elementCounts

simulate :: RuleSet -> PolyPairs -> [PolyPairs]
simulate rules polymer = iterate (step rules) polymer

step :: RuleSet -> PolyPairs -> PolyPairs
step rules polymer = MS.union firsts seconds
  where firsts = MS.map (addFirst rules) polymer
        seconds = MS.map (addSecond rules) polymer

addFirst :: RuleSet -> String -> String
addFirst rules pair = a : c
  where a = pair!!0
        c = rules ! pair

addSecond :: RuleSet -> String -> String
addSecond rules pair = c ++ [a]
  where a = pair!!1
        c = rules ! pair

countElements :: PolyPairs -> M.Map Char Int
countElements pairs = counts
  where firsts = MS.map (!!0) pairs
        seconds = MS.map (!!1) pairs
        elems = S.union (MS.toSet firsts) (MS.toSet seconds)
        counts = M.map ((`div` 2) . (+ 1)) $ MS.toMap $ MS.union firsts seconds


mkPairs :: String -> [String]
mkPairs polymer = map stringify $ zip polymer $ tail polymer
stringify (a, b) = [a, b]

merge :: [a] -> [a] -> [a]
merge [] ys = ys
merge (x:xs) ys = x : (merge ys xs)

-- Parse the input file

inputP = (,) <$> (many1 letter) <* many1 endOfLine <*> rulesP

rulesP = M.fromList <$> ruleP `sepBy` endOfLine
ruleP = (,) <$> many1 letter <* " -> " <*> many1 letter

-- successfulParse :: Text -> (Integer, [Maybe Integer])
successfulParse input = 
  case parseOnly inputP input of
    Left  _err -> ("", M.empty) -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right indata -> indata
