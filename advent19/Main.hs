-- Writeup at https://work.njae.me.uk/2021/12/21/advent-of-code-2021-day-19/

import Data.Text ()
import qualified Data.Text.IO as TIO
import Data.Attoparsec.Text hiding (take, takeWhile)

import Linear (V3(..), (^+^), (^-^))
import qualified Data.Set as S
import qualified Data.MultiSet as MS
import Data.Monoid
import Data.Maybe
import Data.List
import Control.Monad

type Coord = V3 Int

data Scanner = Scanner 
  { scannerName :: Int
  , beacons :: [Coord]
  , transformation :: Endo Coord
  , signature :: MS.MultiSet Int
  }
  deriving (Show)

instance Eq Scanner where
  s1 == s2 = (scannerName s1) == (scannerName s2)

data Reconstruction = Reconstruction 
  { found :: [Scanner] -- these have had the transform applied to the beacons
  , working :: [Scanner] -- these have had the transform applied to the beacons
  , waiting :: [Scanner] -- these are as discovered
  }
  deriving (Show)


-- Transformations

type Transform = Endo Coord

instance Show Transform where
  -- show c = show $ appEndo c (V3 1 2 3)
  show c = show $ appEndo c (V3 0 0 0)

nullTrans = Endo id
rotX = Endo \(V3 x y z) -> V3    x (- z)   y
rotY = Endo \(V3 x y z) -> V3    z    y (- x)
rotZ = Endo \(V3 x y z) -> V3 (- y)   x    z
translate v = Endo (v ^+^)

rotations :: [Transform]
rotations = [a <> b | a <- ras, b <- rbs]
  where ras = [ nullTrans, rotY, rotY <> rotY, rotY <> rotY <> rotY
              , rotZ, rotZ <> rotZ <> rotZ]
        rbs = [nullTrans, rotX, rotX <> rotX, rotX <> rotX <> rotX]

-- Main

main :: IO ()
main = 
  do  text <- TIO.readFile "data/advent19.txt"
      let scanners = successfulParse text
      let rec0 = mkReconstruction scanners
      let rec = reconstruct rec0
      let transScanners = found rec
      print $ part1 transScanners
      print $ part2 transScanners

part1 :: [Scanner] -> Int
part1 scanners = S.size $ S.unions $ map (S.fromList . beacons) scanners

part2 :: [Scanner] -> Int
part2 scanners = maximum [manhattan (a ^-^ b) | a <- origins, b <- origins]
  where extractOrigin sc = appEndo (transformation sc) (V3 0 0 0)
        origins = map extractOrigin scanners
        manhattan (V3 x y z) = (abs x) + (abs y) + (abs z)

sign :: [Coord] -> MS.MultiSet Int
sign bcns = MS.fromList [pythag (a ^-^ b) | a <- bcns, b <- bcns, a < b]
  where pythag (V3 x y z) = x^2 + y^2 + z^2

vagueMatch :: Scanner -> Scanner -> Bool
vagueMatch scanner1 scanner2 = s >= (12 * 11) `div` 2
  where s = MS.size $ MS.intersection (signature scanner1) (signature scanner2)

matchingTransform :: Scanner -> Scanner -> Maybe Transform
matchingTransform s1 s2 = listToMaybe $ matchingTransformAll s1 s2

matchingTransformAll :: Scanner -> Scanner -> [Transform]
matchingTransformAll scanner1 scanner2 = 
  do  let beacons1 = beacons scanner1
      let beacons2 = beacons scanner2
      rot <- rotations
      b1 <- beacons1
      b2 <- beacons2
      let t = b1 ^-^ (appEndo rot b2)
      let translation = translate t
      let transB2 = map (appEndo (translation <> rot)) beacons2
      guard $ (length $ intersect beacons1 transB2) >= 12
      return (translation <> rot)


mkReconstruction :: [Scanner] -> Reconstruction
mkReconstruction (s:ss) = Reconstruction {found = [], working = [s], waiting = ss}

reconstruct :: Reconstruction -> Reconstruction
reconstruct r 
  -- | waiting r == [] = Reconstruction { found = (found r) ++ (working r), working = [], waiting = []}
  | working r == [] = r
  | otherwise = reconstruct $ reconstructStep r

reconstructStep :: Reconstruction -> Reconstruction
reconstructStep Reconstruction{..} = 
  Reconstruction { found = current : found
                 , working = workers ++ newWorkers
                 , waiting = waiting'
                 }
  where (current:workers) = working
        possMatches = filter (vagueMatch current) waiting
        matches = filter (isJust . snd) $ zip possMatches $ map (matchingTransform current) possMatches
        waiting' = waiting \\ (map fst matches)
        newWorkers = map (transformScanner) matches

transformScanner :: (Scanner, Maybe Transform) -> Scanner
transformScanner (Scanner{..}, trans) = 
  Scanner { beacons = map (appEndo $ fromJust trans) beacons
          , transformation = fromJust trans
          , ..}


-- Parse the input file

scannersP = scannerP `sepBy` blankLines
scannerP = scannerify <$> nameP <*> beaconsP
  where 
    scannerify name beacons = 
      Scanner { scannerName = name
              , beacons = beacons
              , transformation = nullTrans
              , signature = sign beacons
              }

nameP = ("--- scanner " *>) decimal <* " ---" <* endOfLine

beaconsP = beaconP `sepBy` endOfLine
beaconP = V3 <$> (signed decimal <* ",") <*> (signed decimal <* ",") <*> (signed decimal)

blankLines = many1 endOfLine

-- successfulParse :: Text -> (Integer, [Maybe Integer])
successfulParse input = 
  case parseOnly scannersP input of
    Left  _err -> [] -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right scanners -> scanners
