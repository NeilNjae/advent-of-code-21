-- Writeup at https://work.njae.me.uk/2021/12/10/advent-of-code-2021-day-10/

import qualified Data.Map.Strict as M
import Data.List

-- data ParseState = ParseState [Char] String -- expected closer stack, remaining text

data ParseResult = Complete String -- remaining text
  | Incomplete [Char] -- remaining closing chars
  | Corrupted [Char] String -- remaining closing chars, remaining text
  deriving (Eq, Ord, Show)

closingChars :: M.Map Char Char
closingChars = M.fromList
  [ ('(', ')')
  , ('[', ']')
  , ('{', '}')
  , ('<', '>')
  ]

main :: IO ()
main = 
  do  text <- readFile "data/advent10.txt"
      let commands = lines text
      let parsedCommands = map parseLine commands
      print $ part1 parsedCommands
      print $ part2 parsedCommands

part1 :: [ParseResult] -> Int
part1 parsedCommands = sum $ map scoreIllegal corruptChar
  where corrupts = filter isCorrupt parsedCommands
        corruptChar = map extractCorrupt corrupts

part2 :: [ParseResult] -> Int
part2 parsedCommands = sortedScores !! (length scores `div` 2)
  where scores = map scoreIncompleteString $ filter isIncomplete parsedCommands
        sortedScores = sort scores

isCorrupt :: ParseResult -> Bool
isCorrupt (Corrupted _ _) = True
isCorrupt _ = False

isIncomplete :: ParseResult -> Bool
isIncomplete (Incomplete _) = True
isIncomplete _ = False

extractCorrupt :: ParseResult -> Char
extractCorrupt (Corrupted _ (c:_)) = c
extractCorrupt _ = ' '

scoreIllegal :: Char -> Int
scoreIllegal ')' = 3
scoreIllegal ']' = 57
scoreIllegal '}' = 1197
scoreIllegal '>' = 25137
scoreIllegal _ = 0

scoreIncomplete :: Char -> Int
scoreIncomplete ')' = 1
scoreIncomplete ']' = 2
scoreIncomplete '}' = 3
scoreIncomplete '>' = 4
scoreIncomplete _ = 0

scoreIncompleteString :: ParseResult -> Int
scoreIncompleteString (Incomplete chars) = foldl' buildScore 0 $ map scoreIncomplete chars
  where buildScore total this = total * 5 + this
scoreIncompleteString _ = 0


parseLine :: String -> ParseResult
parseLine [] = Complete ""
parseLine command = case chunk of
        Complete remaining -> parseLine remaining
        _ -> chunk
  where chunk = parseChunk0 command

parseChunk0 :: String -> ParseResult
parseChunk0 [] = Complete ""
parseChunk0 (next:remaining) = case M.lookup next closingChars of
  Just nextCloser -> parseChunk [nextCloser] remaining
  Nothing -> Corrupted [] (next:remaining)

parseChunk :: [Char] -> String -> ParseResult
parseChunk [] remaining = Complete remaining
parseChunk stack [] = Incomplete stack
parseChunk (closer:stack) (next:remaining)
  | next == closer = parseChunk stack remaining -- expected closing character
  | otherwise = case M.lookup next closingChars of 
          -- next is a new opening character
          Just nextCloser -> parseChunk (nextCloser:closer:stack) remaining
          -- wrong character
          Nothing -> Corrupted (closer:stack) (next:remaining)
