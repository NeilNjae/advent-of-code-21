-- Writeup at https://work.njae.me.uk/2021/12/21/advent-of-code-2021-day-18/

import Data.Text ()
import qualified Data.Text.IO as TIO
import Data.Attoparsec.Text 
import Control.Applicative
import Data.Maybe
import Data.List

data Tree = Pair Tree Tree | Leaf Int
  deriving (Eq)

instance Show Tree where
  show (Leaf n) = show n
  show (Pair l r) = "[" ++ show l ++ "," ++ show r ++ "]"

data Cxt = Top | L Cxt Tree | R Tree Cxt
  deriving (Show, Eq)

-- type Context = [(Direction, Tree)]
-- data Direction = Lft | Rght
--   deriving (Show, Eq)

type Loc = (Tree, Cxt)

main :: IO ()
main = 
  do  text <- TIO.readFile "data/advent18.txt"
      let numbers = successfulParse text
      print $ part1 numbers
      print $ part2 numbers

part1 numbers = magnitude total
  where total = foldl1' snailAdd numbers

part2 numbers = maximum [ magnitude $ snailAdd a b 
                        | a <- numbers, b <- numbers]

magnitude :: Tree -> Int
magnitude (Leaf n) = n
magnitude (Pair a b) = 3 * (magnitude a) + 2 * (magnitude b)


left :: Loc -> Loc
left (Pair l r, c) = (l, L c r)

right :: Loc -> Loc
right (Pair l r, c) = (r, R l c)

top :: Tree -> Loc
top t = (t, Top)

up :: Loc -> Loc
up (t, Top) = (t, Top)
up (t, L c r) = (Pair t r, c)
up (t, R l c) = (Pair l t, c)

upmost :: Loc -> Loc
upmost l@(t, Top) = l
upmost l = upmost (up l)

modify :: Loc -> (Tree -> Tree) -> Loc
modify (t, c) f = (f t, c)


explode :: Tree -> Maybe Tree
explode num = 
  case mp0 of
    Nothing -> Nothing 
    Just _ -> Just num1
  where 
    mp0 = pairAtDepth 4 num
    p0 = fromJust mp0
    ((Pair (Leaf nl) (Leaf nr)), _) = p0
    p1 = case rightmostOnLeft p0 of
      Nothing -> p0
      Just leftReg -> modify leftReg (\(Leaf n) -> Leaf (n + nl))
    p2 = case pairAtDepthC 4 (upmost p1) >>= leftmostOnRight of
      Nothing -> p1
      Just rightReg -> modify rightReg (\(Leaf n) -> Leaf (n + nr))
    p3 = case pairAtDepthC 4 (upmost p2) of
      Nothing -> p2
      Just centrePair -> modify centrePair (\_ -> Leaf 0)
    (num1, _) = upmost p3

pairAtDepth :: Int -> Tree -> Maybe Loc
pairAtDepth n t = pairAtDepthC n (top t)

pairAtDepthC :: Int -> Loc -> Maybe Loc
pairAtDepthC _ (Leaf _, _) = Nothing
pairAtDepthC 0 t@(Pair _ _, _) = Just t
pairAtDepthC n t@(Pair _ _, _) = 
  pairAtDepthC (n - 1) (left t) <|> pairAtDepthC (n - 1) (right t)

rightmostOnLeft :: Loc -> Maybe Loc
rightmostOnLeft (_, Top) = Nothing
rightmostOnLeft t@(_, L c r) = rightmostOnLeft $ up t
rightmostOnLeft t@(_, R l c) = Just $ rightmostNum $ left $ up t

rightmostNum :: Loc -> Loc
rightmostNum t@(Leaf _, _) = t
rightmostNum t@(Pair _ _, _) = rightmostNum $ right t

leftmostOnRight :: Loc -> Maybe Loc
leftmostOnRight (_, Top) = Nothing
leftmostOnRight t@(_, R l c) = leftmostOnRight $ up t
leftmostOnRight t@(_, L c r) = Just $ leftmostNum $ right $ up t

leftmostNum :: Loc -> Loc
leftmostNum t@(Leaf _, _) = t
leftmostNum t@(Pair _ _, _) = leftmostNum $ left t

split :: Tree -> Maybe Tree
split num =
  case mn0 of
    Nothing -> Nothing
    Just _ -> Just num1
  where
    mn0 = splittable num
    n0 = fromJust mn0
    ((Leaf sn), _) = n0
    ln = sn `div` 2
    rn = ln + sn `mod` 2
    n1 = modify n0 (\_ -> Pair (Leaf ln) (Leaf rn))
    (num1, _) = upmost n1

splittable :: Tree -> Maybe Loc
splittable t = splittableC (top t)

splittableC :: Loc -> Maybe Loc
splittableC t@(Leaf n, _)
  | n >= 10 = Just t
  | otherwise = Nothing
splittableC t@(Pair _ _, _) = splittableC (left t) <|> splittableC (right t)

reduce :: Tree -> Tree
reduce num = case explode num <|> split num of
  Nothing -> num
  Just num1 -> reduce num1

snailAdd :: Tree -> Tree -> Tree
snailAdd a b = reduce $ Pair a b


-- Parse the input file

sfNumbersP = sfNumberP `sepBy` endOfLine

sfNumberP = regularP <|> pairP

regularP = Leaf <$> decimal
pairP = Pair <$> ("[" *> sfNumberP) <*> ("," *> sfNumberP) <* "]"

-- successfulParse :: Text -> (Integer, [Maybe Integer])
successfulParse input = 
  case parseOnly sfNumbersP input of
    Left  _err -> [] -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right numbers -> numbers



