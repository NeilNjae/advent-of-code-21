-- Writeup at https://work.njae.me.uk/2021/12/01/advent-of-code-2021-day-1/

import Data.List

main :: IO ()
main = 
  do  numStrs <- readFile "data/advent01.txt"
      let nums = map (read @Int) $ lines numStrs
      print $ part1 nums
      print $ part2 nums

part1 :: [Int] -> Int
part1 = countIncreasing

part2 :: [Int] -> Int
part2 nums = countIncreasing $ map sum windows
  where windows = filter ((== 3) . length) $ map (take 3) $ tails nums

countIncreasing :: [Int] -> Int
countIncreasing nums = length $ filter (uncurry (>)) $ zip (tail nums) nums
