-- Writeup at https://work.njae.me.uk/2021/12/13/advent-of-code-2021-day-12/

import Data.Text ()
import qualified Data.Text.IO as TIO

import Data.Attoparsec.Text
-- import Control.Applicative

import Data.Tuple
import Data.Char
import qualified Data.Map.Strict as M
import Data.Map.Strict ((!))
import qualified Data.Set as S
import Data.Set ((\\))

data Path = Path String         -- current cave
                 [String]       -- caves visited
                 (S.Set String) -- closed set of small caves visited
                 (Maybe String) -- the small cave we've visited twice
  deriving (Eq, Ord, Show)

type PathSet = S.Set Path
type Graph = M.Map String (S.Set String)


main :: IO ()
main = 
  do  text <- TIO.readFile "data/advent12.txt"
      let edges = successfulParse text
      let graph = mkGraph edges
      let paths = allPaths graph (S.singleton (Path "start" [] S.empty Nothing)) S.empty
      print $ part1 paths
      print $ part2 paths

mkGraph :: [(String, String)] -> Graph
mkGraph edges = foldr mkEdge pass1 $ map swap edges
  where pass1 = foldr mkEdge M.empty edges
        mkEdge (here, there) = M.insertWith (S.union) here (S.singleton there)

part1 :: PathSet -> Int
part1 paths = S.size $ S.filter nonReturning paths

part2 :: PathSet -> Int
part2 paths = S.size paths

allPaths :: Graph -> PathSet -> PathSet -> PathSet
allPaths graph agenda results
  | S.null agenda = results
  | otherwise = allPaths graph agenda'' results'
  where (current, agenda') = S.deleteFindMin agenda
        newPaths = extendPath graph current
        agenda'' = S.union agenda' newPaths
        results' = S.union results $ recordResult current

extendPath :: Graph -> Path -> PathSet
extendPath graph (Path current trail visited returned) 
  | current == "end" = S.empty
  | (current == "start") && (current `S.member` visited) = S.empty
  | otherwise = S.union (S.map newPathNovel visitableNovel) 
                        (S.map newPathReturning visitableReturning)
  where neighbours = graph ! current
        visited' = if isSmall current then S.insert current visited else visited
        trail' = (current:trail)
        visitableNovel = neighbours \\ visited -- if we're not returning to a small cave
        visitableReturning = if returned == Nothing 
          then (S.filter isSmall neighbours) `S.intersection` visited -- returning to a small cave already visited
          else S.empty
        newPathNovel next = Path next trail' visited' returned
        newPathReturning next = Path next trail' visited' (Just next)

recordResult :: Path -> PathSet
recordResult path@(Path current _ _ _)
  | current == "end" = S.singleton path
  | otherwise = S.empty

isSmall :: String -> Bool
isSmall = all isLower

nonReturning :: Path -> Bool
nonReturning (Path _ _ _ Nothing) = True
nonReturning (Path _ _ _ (Just _)) = False

-- Parse the input file

graphP = edgeP `sepBy` endOfLine
edgeP = (,) <$> many1 letter <* "-" <*> many1 letter

-- successfulParse :: Text -> (Integer, [Maybe Integer])
successfulParse input = 
  case parseOnly graphP input of
    Left  _err -> [] -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right graph -> graph
