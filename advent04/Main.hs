-- Writeup at https://work.njae.me.uk/2021/12/04/advent-of-code-2021-day-4/

import Data.Text ()
import qualified Data.Text.IO as TIO

import Data.Attoparsec.Text
import Control.Applicative

import Data.List

type Square = [[Int]]

data BingoNum = BingoNum Int Bool
  deriving (Eq, Show)

type BingoSquare = [[BingoNum]]

data BingoState = BingoState Int [BingoSquare]
  deriving (Eq, Show)



main :: IO ()
main = 
  do  text <- TIO.readFile "data/advent04.txt"
      let (nums, rawSquares) = successfulParse text
      let squares = map mkSquare rawSquares
      print $ part1 nums squares
      print $ part2 nums squares

part1 :: [Int] -> [BingoSquare] -> Int
part1 callNums squares = finalCalled * winningSum
  where allSteps = scanl' bingoStep (BingoState 0 squares) callNums
        BingoState finalCalled finalSquares = head $ dropWhile (not . hasCompletedSquare) allSteps
        winningSquare = head $ filter completed finalSquares
        winningSum = unmarkedSum winningSquare

part2 :: [Int] -> [BingoSquare] -> Int
part2 callNums squares = finalCalled * winningSum
  where allSteps = scanl' pruningBingoStep (BingoState 0 squares) callNums
        BingoState finalCalled finalSquares = 
          head $ dropWhile (not . hasCompletedSquare) 
               $ dropWhile manyRemainingSquares allSteps
        winningSquare = head finalSquares
        winningSum = unmarkedSum winningSquare


bingoStep :: BingoState -> Int -> BingoState
bingoStep (BingoState _ squares) caller = BingoState caller squares'
  where squares' = map (callSquare caller) squares

pruningBingoStep :: BingoState -> Int -> BingoState
pruningBingoStep (BingoState _ squares) caller = BingoState caller squares''
  where squares' = filter (not . completed) squares
        squares'' = map (callSquare caller) squares'

hasCompletedSquare :: BingoState -> Bool
hasCompletedSquare (BingoState _n squares) = any completed squares

unmarkedSum :: BingoSquare -> Int
unmarkedSum bingoSquare = 
  sum [value bn | r <- bingoSquare, bn <- r, (not $ isCalled bn)]

manyRemainingSquares :: BingoState -> Bool
manyRemainingSquares (BingoState _ squares) = (length squares) > 1


mkBingoNum :: Int -> BingoNum
mkBingoNum n = BingoNum n False

forceCall :: BingoNum -> BingoNum
forceCall (BingoNum n _) = BingoNum n True

call :: Int -> BingoNum -> BingoNum
call num (BingoNum target called) 
  | num == target = BingoNum target True
  | otherwise = BingoNum target called

isCalled :: BingoNum -> Bool
isCalled (BingoNum _ c) = c

value :: BingoNum -> Int
value (BingoNum n _) = n

mkSquare :: Square -> BingoSquare
mkSquare = map (map mkBingoNum)

callSquare :: Int -> BingoSquare -> BingoSquare
callSquare n = map (map (call n))

completed :: BingoSquare -> Bool
completed sq = (any completedRow sq) || (any completedRow $ transpose sq)

completedRow :: [BingoNum] -> Bool
completedRow = all isCalled

-- Parse the input file

bingoP = (,) <$> calledP <*> (blankLines *> squaresP)

calledP = decimal `sepBy` ","

squaresP = squareP `sepBy` blankLines
squareP = rowP `sepBy` endOfLine
rowP = paddedDecimal `sepBy1` " "

-- paddedDecimal :: Parser Text Int
paddedDecimal = (many " ") *> decimal

blankLines = many1 endOfLine

-- successfulParse :: Text -> (Integer, [Maybe Integer])
successfulParse input = 
  case parseOnly bingoP input of
    Left  _err -> ([], []) -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right bingo -> bingo
