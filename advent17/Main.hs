-- Writeup at https://work.njae.me.uk/2021/12/19/advent-of-code-2021-day-17/

import Data.Text ()
import qualified Data.Text.IO as TIO
import Data.Attoparsec.Text hiding (take, takeWhile)

import Control.Lens
import Linear (V2(..), (^+^), _x, _y)
import Data.Ix

type Coord = V2 Int
type Bounds = (Coord, Coord)

data Probe = Probe {_pos :: Coord, _vel :: Coord}
  deriving (Show, Eq)
makeLenses ''Probe



main :: IO ()
main = 
  do  text <- TIO.readFile "data/advent17.txt"
      let target = successfulParse text
      print $ part1 target
      print $ part2 target

part1 :: Bounds -> Int
part1 = findYMax 

part2 :: Bounds -> Int
part2 target@(V2 _minXT minYT, V2 maxXT _maxYT) = 
  length $ filter (hits target) trajectories
  where yMax = findYMax target
        viable = (V2 0 minYT, V2 maxXT yMax)
        launches = [Probe {_pos = V2 0 0, _vel = V2 x y} 
                   | x <- [1..maxXT], y <- [minYT..(abs minYT)]
                   ]
        trajectories = map (simulate viable) launches

findYMax :: Bounds -> Int
findYMax (V2 _ y, _) = y' * (y' - 1) `div` 2
  where y' = abs y

step :: Probe -> Probe
step probe = probe & pos .~ (probe ^. pos ^+^ probe ^. vel) & vel .~ vel'
  where vel' = V2 (max 0 (vx - 1)) (vy - 1)
        V2 vx vy = probe ^. vel
        -- v = probe ^. vel
        -- vel' = v & _x .~ (max 0 ((v ^. _x) - 1)) & _y .~ ((v ^. _y) - 1)

simulate :: Bounds -> Probe -> [Probe]
simulate viable = takeWhile (within viable) . iterate step

within :: Bounds -> Probe -> Bool
within limits probe = inRange limits (probe ^. pos)

hits :: Bounds -> [Probe] -> Bool
hits target = any (within target)


-- Parse the input file

targetP = boundify <$> ("target area: x=" *> regionP) <*> (", y=" *> regionP)
  where boundify (x1, x2) (y1, y2) = (V2 x1 y1, V2 x2 y2)

regionP = (,) <$> (signed decimal <* "..") <*> signed decimal

-- successfulParse :: Text -> (Integer, [Maybe Integer])
successfulParse input = 
  case parseOnly targetP input of
    Left  _err -> (V2 0 0, V2 0 0) -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right target -> target
