-- Writeup at https://work.njae.me.uk/2021/12/07/advent-of-code-2021-day-7/

import Data.List.Split

main :: IO ()
main = 
  do  text <- readFile "data/advent07.txt"
      let subs = map (read @Int) $ splitOn "," text
      print $ part1 subs
      print $ part2 subs

part1 = bestFuel loss1
part2 = bestFuel loss2

bestFuel :: ([Int] -> Int -> Int) -> [Int] -> Int
bestFuel loss subs = loss subs best
  where meanSub = (sum subs) `div` (length subs)
        best = gradientDescend loss subs meanSub

loss1 :: [Int] -> Int -> Int
loss1 subs target = sum $ map diff subs
  where diff s = abs (target - s)

loss2 :: [Int] -> Int -> Int
loss2 subs target = sum $ map triangleDiff subs
  where diff s = abs (target - s)
        triangleDiff s = ((diff s) * ((diff s) + 1)) `div` 2

gradientDescend :: ([Int] -> Int -> Int) -> [Int] -> Int -> Int
gradientDescend loss subs guess = 
  if | lossLower  < lossHere -> gradientDescend loss subs (guess - 1)
     | lossHigher < lossHere -> gradientDescend loss subs (guess + 1)
     | otherwise -> guess
  where lossHere   = loss subs guess
        lossLower  = loss subs (guess - 1)
        lossHigher = loss subs (guess + 1)
