-- Writeup at https://work.njae.me.uk/2021/12/06/advent-of-code-2021-day-6/

import Data.List
import Data.List.Split
import qualified Data.IntMap.Strict as M

type Shoal = M.IntMap Integer

main :: IO ()
main = 
  do  text <- readFile "data/advent06.txt"
      let fish = map read $ splitOn "," text
      let shoal = mkShoal fish
      let generations = iterate generation shoal
      print $ part1 generations
      print $ part2 generations

part1 :: [Shoal] -> Integer
part1 = countInGeneration 80

part2 :: [Shoal] -> Integer
part2 = countInGeneration 256

countInGeneration :: Int -> [Shoal] -> Integer
countInGeneration n generations = sum $ M.elems (generations!!n)

generation :: Shoal -> Shoal
generation shoal = M.union (age shoal) (birth shoal)

age :: Shoal -> Shoal
age shoal = M.insert 6 (was0 + was7) shoal'
  where shoal' = M.mapKeys ageFish shoal
        was0 = M.findWithDefault 0 0 shoal
        was7 = M.findWithDefault 0 7 shoal

ageFish :: Int -> Int
ageFish 0 = 6
ageFish n = n - 1

birth :: Shoal -> Shoal
birth shoal = M.singleton 8 parents
  where parents = M.findWithDefault 0 0 shoal

mkShoal :: [Int] -> Shoal
mkShoal = M.fromList 
  . map (\g -> (head g, fromIntegral $ length g)) 
  . group 
  . sort
