-- Writeup at https://work.njae.me.uk/2021/12/02/advent-of-code-2021-day-2/

import Data.Text ()
import qualified Data.Text.IO as TIO

import Data.Attoparsec.Text
import Control.Applicative

type Course = [Command]
data Command =   Forward Int
               | Up Int
               | Down Int
               deriving (Eq, Show)

data Position = Position Int Int -- forward, depth
  deriving (Eq, Show)               

data AimedPosition = AimedPosition Int Int Int -- forward, depth, aim
  deriving (Eq, Show)               

main :: IO ()
main = 
  do  text <- TIO.readFile "data/advent02.txt"
      let course = successfulParse text
      print $ part1 course
      print $ part2 course

part1 :: Course -> Int
part1 course = finalH * finalD
  where (Position finalH finalD) = followCourse course

part2 :: Course -> Int
part2 course = finalH * finalD
  where (AimedPosition finalH finalD _) = followAimedCourse course

followCourse :: Course -> Position
followCourse = foldl courseStep (Position 0 0)

courseStep :: Position -> Command -> Position
courseStep (Position h d) (Forward n) = Position (h + n) d
courseStep (Position h d) (Up n)      = Position h (d - n)
courseStep (Position h d) (Down n)    = Position h (d + n)

followAimedCourse :: Course -> AimedPosition
followAimedCourse = foldl courseAimedStep (AimedPosition 0 0 0)

courseAimedStep :: AimedPosition -> Command -> AimedPosition
courseAimedStep (AimedPosition h d a) (Forward n) = AimedPosition (h + n) (d + a * n) a
courseAimedStep (AimedPosition h d a) (Up n)      = AimedPosition h d (a - n)
courseAimedStep (AimedPosition h d a) (Down n)    = AimedPosition h d (a + n)


-- Parse the input file

courseP = commandP `sepBy` endOfLine
commandP = forwardP <|> upP <|> downP

forwardP = Forward <$> ("forward " *> decimal)
upP = Up <$> ("up " *> decimal)
downP = Down <$> ("down " *> decimal)

-- successfulParse :: Text -> (Integer, [Maybe Integer])
successfulParse input = 
  case parseOnly courseP input of
    Left  _err -> [] -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right course -> course
