---
title: "Advent of Code 2020"
output: html_document
css: modest.css
---
Code to solve the [Advent of Code](http://adventofcode.com/2020/) puzzles. This year, I'm using the puzzles to develop my skills in [Haskell](https://wiki.haskell.org/Haskell). I'm writing up a [commentary on these puzzles and my solutions](https://work.njae.me.uk/tag/advent-of-code/) on my blog.

[Learn you a Haskell](http://learnyouahaskell.com/chapters), [Introduction to Haskell 98](https://www.haskell.org/tutorial/index.html), and [Hackage](https://hackage.haskell.org/) are good resources.

The [Cabal user guide](https://cabal.readthedocs.io/en/latest/index.html) and [How I Start: Haskell](http://howistart.org/posts/haskell/1/) are good sources of using the tools. 

# Toolchain

Install Ghcup following [the instructions](https://www.haskell.org/ghcup/install/#installation), making sure to load the updated environment with 

```bash
source /home/neil/.ghcup/env
```

and then set the default GHC to use with `ghcup set ghc 9.0.1` .

Install [Haskell Language Server](https://haskell-language-server.readthedocs.io/en/latest/configuration.html) for Sublime Text


## Creating the repository and project
Create the repository as normal: create the project in Gitolite, clone it, and insert the `.gitignore` and `README.md` files.

There's one package per day, with the code for each package in sub-directories of the root directory. 

Create the basic `cabal` project. 

```
cabal init
```

Modify the `advent-of-code21.cabal` file as needed, such as updating the Cabal version and writing the `common` stanzas. 

## Creating subsequent days

Each day lives in a separate directory, with code in the `src` directory. 

Compile with
```
cabal build
```
or 
```
cabal build advent01
```

Run with
```
cabal run advent01
```

If you want to pass in additional RTS parameters, do it like this:
```
cabal run advent01 -- +RTS -K0 -RTS
```

Run interactively with
```
cabal repl advent01
```
or 
```
stack ghci advent01:exe:advent01
```
if the first form is ambiguous. 

## Profiling

To profile, use 

```
cabal run advent01 --enable-profiling -- +RTS -N -p -s -hT
```

Or, you can simplify the RTS options by adding them to a new stanza in the cabal file:

```
executable advent01prof
  import: common-extensions, build-directives
  main-is: advent01/Main.hs
  build-depends: text, containers, linear, array, pqueue, mtl, lens
  ghc-options:         -O2 
                       -Wall 
                       -threaded 
                       -rtsopts "-with-rtsopts=-N -p -s -hT"
```

then running 

```
cabal run advent01prof --enable-profiling
```


Generate the profile graph with
```
hp2ps -M advent01.hp
```


# Packages

Packages I used a lot:

* [Containers](https://hackage.haskell.org/package/containers) (and some [better documentation](https://haskell-containers.readthedocs.io/en/latest/intro.html)); [Unordered containers](https://hackage.haskell.org/package/unordered-containers) is a mostly-equivalent alternative.
* [Attoparsec](https://hackage.haskell.org/package/attoparsec) (and [Megaparsec](https://hackage.haskell.org/package/megaparsec), and [ReadP](https://hackage.haskell.org/package/base-4.14.1.0/docs/Text-ParserCombinators-ReadP.html) once).

There are somewhat decent [tutorials on Megaparsec](https://markkarpov.com/tutorial/megaparsec.html) and [Attoparsec](https://www.schoolofhaskell.com/school/starting-with-haskell/libraries-and-frameworks/text-manipulation/attoparsec).

Packages I didn't use much, but need to remember:

* [Arithmoi](https://hackage.haskell.org/package/arithmoi) for number theory
* [Pointed List](https://hackage.haskell.org/package/pointedlist-0.6.1) for zipper lists (sometimes circular)
* [Vector](https://hackage.haskell.org/package/vector) for array-like things
* [Linear](https://hackage.haskell.org/package/linear) for coordinate-vector like things
* [Grid](https://hackage.haskell.org/package/grid) for 2-d grids
* [Graph-wrapper](https://hackage.haskell.org/package/graph-wrapper) for graphs
* [Lens](https://hackage.haskell.org/package/lens) (and a [summary of operators](https://github.com/ekmett/lens/wiki/Operators)). I didn't use these much this year, but did a lot last year.
* [RWS](https://hackage.haskell.org/package/mtl-2.2.2/docs/Control-Monad-RWS-Lazy.html) (Reader-Writer-State monad stack); again, used a lot last year but not this year
* [Monad loops](https://hackage.haskell.org/package/monad-loops-0.4.3/docs/Control-Monad-Loops.html), and [a description](https://conscientiousprogrammer.com/blog/2015/12/11/24-days-of-hackage-2015-day-11-monad-loops-avoiding-writing-recursive-functions-by-refactoring/)
* [Replace-Megaparsec](https://github.com/jamesdbrock/replace-megaparsec), for using Mpc for all sorts of things traditionally done with regex substitutions.

# Readme

Build this readme file wth
```
pandoc -s README.md > README.html
```

(Using the [Modest style](https://github.com/markdowncss/modest).)
