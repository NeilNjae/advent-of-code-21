-- Writeup at https://work.njae.me.uk/2021/12/05/advent-of-code-2021-day-5/

import Data.Text ()
import qualified Data.Text.IO as TIO

import Data.Attoparsec.Text hiding (take)
import Control.Applicative

import qualified Data.Map.Strict as M
import Linear (V2(..), (^+^))


type Point = V2 Int
type Grid = M.Map Point Int

data Line = Line Point Point deriving (Eq, Show)


main :: IO ()
main = 
  do  text <- TIO.readFile "data/advent05.txt"
      let trace = successfulParse text
      print $ part1 trace
      print $ part2 trace

part1 trace = M.size $ M.filter (> 1) diagram
  where hvLines = map expandLine $ filter isHorizOrVert trace
        diagram = addLines M.empty hvLines

part2 trace = M.size $ M.filter (> 1) diagram
  where allLines = map expandLine trace
        diagram = addLines M.empty allLines


isHoriz :: Line -> Bool
isHoriz (Line (V2 x0 _y0) (V2 x1 _y1)) = x0 == x1

isVert :: Line -> Bool
isVert (Line (V2 _x0 y0) (V2 _x1 y1)) = y0 == y1

isHorizOrVert :: Line -> Bool
isHorizOrVert line = (isHoriz line) || (isVert line)

expandLine :: Line -> [Point]
expandLine line@(Line p0 _p1) = take numPoints $ iterate (^+^ delta) p0
  where numPoints = findLen line + 1
        delta = findDelta line

findLen :: Line -> Int
findLen (Line (V2 x0 y0) (V2 x1 y1)) = 
  max (abs (x1 - x0)) (abs (y1 - y0))

findDelta :: Line -> Point
findDelta (Line (V2 x0 y0) (V2 x1 y1)) = (V2 dx dy)
  where dx = signum (x1 - x0)
        dy = signum (y1 - y0)

addLines :: Grid -> [[Point]] -> Grid
addLines = foldr insertLine 
  where insertPoint p d = M.insertWith (+) p 1 d
        insertLine l d = foldr insertPoint d l


-- Parse the input file

traceP = lineP `sepBy` endOfLine
lineP = Line <$> (pointP <* " -> ") <*> pointP
pointP = V2 <$> (decimal <* ",") <*> decimal

-- successfulParse :: Text -> (Integer, [Maybe Integer])
successfulParse input = 
  case parseOnly traceP input of
    Left  _err -> [] -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right trace -> trace
