-- Writeup at https://work.njae.me.uk/2022/04/24/advent-of-code-2021-day-25/

import qualified Data.Map.Strict as M
import Data.Map.Strict ((!), (\\), (!?))
import Linear (V2(..), (^+^))
import Data.List (unfoldr)

type Coord = V2 Int -- r, c
data Grid = Grid (Coord, Coord) (M.Map Coord Cucumber)
  deriving (Eq, Show)

data Cucumber = Eastwards | Southwards
  deriving (Eq)

instance Show Cucumber where
  show Eastwards = ">"  
  show Southwards = "v"

main :: IO ()
main = 
  do  text <- readFile "data/advent25.txt"
      let grid = mkGrid text
      print $ (1 +) $ length $ simulate grid

mkGrid :: String -> Grid
mkGrid text = Grid (V2 0 0, V2 maxR maxC) 
    ( M.fromList 
      [ (V2 r c, mkCucubmer r c) 
      | r <- [0..maxR], c <- [0..maxC]
      , isCucumber r c
      ]
    )
  where rows = lines text
        maxR = length rows - 1
        maxC = (length $ head rows) - 1
        isCucumber r c = ((rows !! r) !! c) `elem` (">v" :: String)
        mkCucubmer r c = if (rows !! r) !! c == '>' then Eastwards else Southwards

delta :: Cucumber -> Coord
delta Eastwards = V2 0 1
delta Southwards = V2 1 0

wrap :: (Coord, Coord) -> Coord -> Coord
wrap bounds@(V2 r0 c0, V2 r1 c1) (V2 r c)
  | r > r1 = wrap bounds $ V2 r0 c
  | r < r0 = wrap bounds $ V2 r1 c
  | c > c1 = wrap bounds $ V2 r c0
  | c < c0 = wrap bounds $ V2 r c1
  | otherwise = V2 r c

ahead :: Grid -> Coord -> Coord
ahead (Grid bounds cucumbers) here = wrap bounds (here ^+^ (delta c))
  where c = cucumbers ! here

vacant :: Grid -> Coord -> Bool
vacant (Grid _ cucumbers) here = M.notMember here cucumbers

canMove :: Grid -> Grid
canMove grid@(Grid bounds cucumbers) = Grid bounds $ M.filterWithKey openAhead cucumbers
  where openAhead here _ = vacant grid $ ahead grid here

blocked :: Grid -> Bool
blocked grid = M.null cucumbers 
  where Grid _ cucumbers = canMove grid

eastFacing, southFacing :: Grid -> Grid
eastFacing (Grid bounds cucumbers) = Grid bounds $ M.filter (== Eastwards) cucumbers
southFacing (Grid bounds cucumbers) = Grid bounds $ M.filter (== Southwards) cucumbers

advanceEastwards, advanceSouthwards :: Grid -> Grid
advanceEastwards = advance eastFacing
advanceSouthwards = advance southFacing

advance :: (Grid -> Grid) -> Grid -> Grid
advance facing grid@(Grid bounds cucumbers) = Grid bounds $ M.union cannotMove advanced
  where Grid _ advancing = facing $ canMove grid
        cannotMove = cucumbers \\ advancing
        advanced = M.fromList $ map advanceOne $ M.toAscList advancing
        advanceOne (here, c) = (ahead grid here, c)

step :: Grid -> Grid
step = advanceSouthwards . advanceEastwards 

maybeStep :: Grid -> Maybe (Grid, Grid)
maybeStep grid
  | blocked grid = Nothing
  | otherwise = Just (grid', grid')
  where grid' = step grid

simulate :: Grid -> [Grid]
simulate grid = unfoldr maybeStep grid

showGrid :: Grid -> String
showGrid (Grid (V2 minR minC, V2 maxR maxC) cucumbers) = 
  unlines $ [ concat [showCucumber (V2 r c) | c <- [minC..maxC] ] | r <- [minR..maxR] ]
  where showCucumber here = maybe "." show $ cucumbers !? here
