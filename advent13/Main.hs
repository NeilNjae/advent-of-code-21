-- Writeup at https://work.njae.me.uk/2021/12/13/advent-of-code-2021-day-13/

import Data.Text ()
import qualified Data.Text.IO as TIO

import Data.Attoparsec.Text
import Control.Applicative

import qualified Data.Set as S
-- import Data.Set ((\\))
import Linear (V2(..), (^+^))
import Data.List


type Coord = V2 Int
type Sheet = S.Set Coord

data Axis = X | Y
  deriving (Eq, Ord, Show)

data Fold = Fold Axis Int
  deriving (Eq, Ord, Show)

main :: IO ()
main = 
  do  text <- TIO.readFile "data/advent13.txt"
      let (sheet, folds) = successfulParse text
      print $ part1 sheet folds
      putStrLn $ part2 sheet folds

part1 :: Sheet -> [Fold] -> Int
part1 sheet folds = S.size $ doFold sheet $ head folds

part2 :: Sheet -> [Fold] -> String
part2 sheet folds = showSheet foldedSheet
  where foldedSheet = foldl' doFold sheet folds

doFold :: Sheet -> Fold -> Sheet
doFold sheet (Fold X i) = S.union left foldedRight
  where (left, right) = S.partition ((<= i) . getX) sheet
        foldedRight = S.map foldDot right
        foldDot (V2 x y) = V2 (i - (x - i)) y

doFold sheet (Fold Y i) = S.union top foldedBottom
  where (top, bottom) = S.partition ((<= i) . getY) sheet
        foldedBottom = S.map foldDot bottom
        foldDot (V2 x y) = V2 x (i - (y - i))

getX :: Coord -> Int
getX (V2 x _) = x

getY :: Coord -> Int
getY (V2 _ y) = y


showSheet :: Sheet -> String
showSheet sheet = unlines [ concat [showPoint (V2 x y) | x <- ([0..maxX] :: [Int]) 
                                   ] | y <- ([0..maxY] :: [Int]) ]
  where showPoint here = if here `S.member` sheet then "█" else " "
        maxX = S.findMax $ S.map getX sheet
        maxY = S.findMax $ S.map getY sheet


-- Parse the input file

inputP = (,) <$> sheetP <* many1 endOfLine <*> foldsP

sheetP = S.fromList <$> dotP `sepBy` endOfLine
dotP = V2 <$> decimal <* "," <*> decimal

foldsP = foldP `sepBy` endOfLine
foldP = Fold <$> ("fold along " *> axisP) <* "=" <*> decimal

axisP = ("x" *> pure X) <|> ("y" *> pure Y)

-- successfulParse :: Text -> (Integer, [Maybe Integer])
successfulParse input = 
  case parseOnly inputP input of
    Left  _err -> (S.empty, []) -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right indata -> indata
