-- Writeup at https://work.njae.me.uk/2021/12/09/advent-of-code-2021-day-8/


import Data.Text ()
import qualified Data.Text.IO as TIO

import Data.Attoparsec.Text
-- import Control.Applicative

import Data.List 
import qualified Data.Map.Strict as M
import Data.Map.Strict ((!))
import qualified Data.Set as S

data Display = Display [String] [String] -- patterns, output
  deriving (Eq, Show)

data Segment = Seg1 | Seg2 | Seg3 | Seg4 | Seg5 | Seg6 | Seg7  
  deriving (Eq, Ord, Show, Enum, Bounded)

type Encoding = M.Map Char Segment
type DigitSegments = M.Map (S.Set Segment) Char

-- some constants
segmentNames :: [Char]
segmentNames = "abcdefg"

segments :: [Segment]
segments = [Seg1 .. Seg7]

digitSegments :: DigitSegments
digitSegments = M.fromList
  [ (S.fromList [Seg1, Seg2, Seg3, Seg5, Seg6, Seg7], '0')
  , (S.fromList [Seg3, Seg6], '1')
  , (S.fromList [Seg1, Seg3, Seg4, Seg5, Seg7], '2')
  , (S.fromList [Seg1, Seg3, Seg4, Seg6, Seg7], '3')
  , (S.fromList [Seg2, Seg3, Seg4, Seg6], '4')
  , (S.fromList [Seg1, Seg2, Seg4, Seg6, Seg7], '5')
  , (S.fromList [Seg1, Seg2, Seg4, Seg5, Seg6, Seg7], '6')
  , (S.fromList [Seg1, Seg3, Seg6], '7')
  , (S.fromList [Seg1, Seg2, Seg3, Seg4, Seg5, Seg6, Seg7], '8')
  , (S.fromList [Seg1, Seg2, Seg3, Seg4, Seg6, Seg7], '9')
  ]


main :: IO ()
main = 
  do  text <- TIO.readFile "data/advent08.txt"
      let displays = successfulParse text
      print $ part1 displays
      print $ part2 displays

part1 :: [Display] -> Int
part1 displays = sum $ map countUniques displays

part2 :: [Display] -> Int
part2 displays = sum $ map decodeOneDisplay displays

countUniques :: Display -> Int
countUniques (Display _ outputs) = length uniqLens
  where outLens = map length outputs
        uniqLens = outLens `intersect` uniqueLengths

uniqueLengths :: [Int]
uniqueLengths = [2, 3, 4, 7]

decodeOneDisplay :: Display -> Int
decodeOneDisplay display = findCode allocation display
  where allocation = allocate display



allocate :: Display -> Encoding
allocate (Display examples _) = head $ validEncodings
  where allEncodings = map (\segs -> M.fromList $ zip segmentNames segs) 
                            $ permutations segments
        validEncodings = filter (isValidEncoding examples) allEncodings

segmentsOfSignal :: Encoding -> [Char] -> S.Set Segment
segmentsOfSignal encoding signal = S.fromList $ map (encoding ! ) signal

isValidEncoding :: [[Char]] -> Encoding -> Bool
isValidEncoding examples encoding = 
  all (\e -> M.member e digitSegments) exampleSegments
  where exampleSegments = map (segmentsOfSignal encoding) examples


findDigit :: Encoding -> [Char] -> Char
findDigit encoding code = digitSegments ! (segmentsOfSignal encoding code)

findDigits :: Encoding -> [[Char]] -> [Char]
findDigits encoding codes = map (findDigit encoding) codes

findCode :: Encoding -> Display -> Int
findCode encoding (Display _ codes) = read $ findDigits encoding codes

-- Parse the input file

displaysP = displayP `sepBy` endOfLine
displayP = Display <$> (patternsP <* " | ") <*> patternsP

patternsP = patternP `sepBy` " "
patternP = many1 letter

-- successfulParse :: Text -> (Integer, [Maybe Integer])
successfulParse input = 
  case parseOnly displaysP input of
    Left  _err -> [] -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right displays -> displays
