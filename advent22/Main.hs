-- Writeup at https://work.njae.me.uk/2021/12/29/advent-of-code-2021-day-22/

import Data.Text ()
import qualified Data.Text.IO as TIO
import Data.Attoparsec.Text -- hiding (take, takeWhile)
import Control.Applicative

import Linear
import Control.Lens
import Data.List

type Coord = V3 Int

data Parity = On | Off deriving (Eq, Ord, Show)

data Cuboid = Cuboid 
  { _bounds :: (Coord, Coord)
  , _parity :: Parity
  , _time :: Int
  }
  deriving (Ord, Eq, Show)
makeLenses ''Cuboid

-- Main

main :: IO ()
main = 
  do  text <- TIO.readFile "data/advent22.txt"
      let cuboids = successfulParse text
      print $ part1 cuboids
      print $ part2 cuboids

part1 cuboids = sweepX (filter isLocal cuboids)
part2 cuboids = sweepX cuboids

isLocal :: Cuboid -> Bool
isLocal cuboid = all (>= (- 50)) ls && all (<= 50) hs
  where ls = [cuboid ^. bounds . _1 . c |  c <- [_x, _y, _z]] :: [Int]
        hs = [cuboid ^. bounds . _2 . c |  c <- [_x, _y, _z]] :: [Int]

straddles :: (Lens' (V3 Int) Int) -> Int -> Cuboid -> Bool
straddles f here cuboid = 
  ((cuboid ^. bounds . _1 . f) <= here) && ((cuboid ^. bounds . _2 . f) >= here)

events :: (Lens' (V3 Int) Int) -> [Cuboid] -> [Int]
events f cuboids = nub $ sort $ ls ++ hs
  where ls = map (^. bounds . _1 . f) cuboids
        hs = map ((+1) . (^. bounds . _2 . f)) cuboids

isActive :: [Cuboid] -> Bool
isActive [] = False
isActive cs = ((last scs) ^. parity) == On
  where scs = sortOn (^. time) cs

sweepX :: [Cuboid] -> Int
sweepX cuboids = sum $ map (volumeSize cuboids) $ segment evs
  where evs = events _x cuboids

volumeSize :: [Cuboid] -> (Int, Int) -> Int
volumeSize cuboids (here, there) = (sweepY cuboidsHere) * (there - here)
  where cuboidsHere = filter (straddles _x here) cuboids

-- assume for a given x
sweepY :: [Cuboid] -> Int
sweepY cuboids = sum $ map (areaSize cuboids) $ segment evs
  where evs = events _y cuboids

areaSize :: [Cuboid] -> (Int, Int) -> Int
areaSize cuboids (here, there) = (sweepZ cuboidsHere) * (there - here)
  where cuboidsHere = filter (straddles _y here) cuboids

-- assume for a given x and y.
sweepZ :: [Cuboid] -> Int
sweepZ cuboids = sum $ map (segmentSize cuboids) $ segment evs
  where evs = events _z cuboids

segmentSize :: [Cuboid] -> (Int, Int) -> Int
segmentSize cuboids (here, there) 
  | isActive $ filter (straddles _z here) cuboids = (there - here)
  | otherwise = 0

segment :: [Int] -> [(Int, Int)]
-- segment evs = if null evs then [] else zip evs $ tail evs
segment [] = []
segment evs@(_ : tevs) = zip evs tevs

-- Parse the input file

cuboidsP = timeify <$> cuboidP `sepBy` endOfLine
  where timeify cuboids = map (\(c, n) -> c & time .~ n) $ zip cuboids [0..]

cuboidP = cubify <$> (partiyP <* " ") <*> (boundsP `sepBy` ",")
  where cubify p ranges = 
          Cuboid { _parity = p
                 , _bounds = ( vecify (map fst ranges)
                             , vecify (map snd ranges)
                             )
                 , _time = 0
                 }
        vecify [c1, c2, c3] = V3 c1 c2 c3

-- partiyP = ("on" *> pure On) <|> ("off" *> pure Off)
partiyP = (On <$ "on") <|> (Off <$ "off")

boundsP = (,) <$> (("x" <|> "y" <|> "z") *> "=" *> signed decimal) <*> (".." *> signed decimal)

-- successfulParse :: Text -> (Integer, [Maybe Integer])
successfulParse input = 
  case parseOnly cuboidsP input of
    Left  _err -> [] -- TIO.putStr $ T.pack $ parseErrorPretty err
    Right cuboids -> cuboids
